import React, { useState } from "react";
import Pagination from "../../Components/Pagination";

export default {
  title: "UI/Components/Pagination",
  component: Pagination
};

const Template = (props) => {
  const [page, setPage] = useState(0);

  return <Pagination totalPages={10} {...{ page, setPage }} />;
};

export const Default = Template.bind({});
Default.args = {
  totalPages: 10,
  setPage: () => {},
  page: 0
};
