import React from "react";
import Carousel from "react-bootstrap/Carousel";

export default {
  title: "UI/Components/Carousel",
  component: Carousel
};

const Template = (props) => (
  <div style={{ width: 835 }}>
    <Carousel>
      {Array.from({ length: 1 }, (_, i) => (
        <Carousel.Item key={i}>
          <img
            className="d-block w-100 rounded"
            src="https://via.placeholder.com/835x450"
            alt={`Slide ${i + 1}`}
          />
          <Carousel.Caption>
            <h3>First slide label</h3>
            <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
          </Carousel.Caption>
        </Carousel.Item>
      ))}
    </Carousel>
  </div>
);

export const Default = Template.bind({});
Default.args = {};
