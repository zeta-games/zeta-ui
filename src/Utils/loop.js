const loop = (length) => Array.from({ length }, (_, i) => i);

export default loop;
