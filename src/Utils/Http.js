import { setGlobal } from "reactn";
import { toast } from "react-toastify";
import axios from "axios";

/**
 * variables
 */
const rootState = {
  token: null,
  user: null
};

/**
 * functions
 */
export const Http = axios.create({
  baseURL: process.env.REACT_APP_API_BASEURL,
  timeout: 45000,
  headers: {
    "X-Requested-With": "XMLHttpRequest",
    "Content-Type": "application/json",
    Accept: "application/json"
  }
});

Http.interceptors.request.use((config) => {
  if (!config?.unauthenticated) {
    const { token } =
      JSON.parse(
        window.localStorage.getItem(process.env.REACT_APP_STORAGE_KEY)
      ) || {};

    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }
  }

  return config;
});

Http.interceptors.response.use(
  (response) => response?.data,
  (error) => {
    if (error.response?.status) {
      if (error.response.status === 401) {
        window.localStorage.removeItem("token");
        setGlobal(rootState);
      }

      if (error.response.status === 500) {
        toast.error("A server error occured");
      }

      return Promise.reject(
        error.response.data.fields || error.response.data.message
      );
    }

    return Promise.reject(error);
  }
);

export default Http;
