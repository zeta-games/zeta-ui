import React from "react";
import PropTypes from "prop-types";

const propTypes = {
  /**
   * @default '24'
   */
  size: PropTypes.number,

  /**
   * @default '#000'
   */
  color: PropTypes.string
};

const defaultProps = {
  size: 24,
  color: "var(--bs-blue-10)"
};

const Template = ({ size, color, ...props }) => (
  <svg
    fill="none"
    width={size}
    height={size}
    viewBox="0 0 24 24"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      stroke={color}
      strokeWidth="1.5"
      d="M20 6L9 17L4 12"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);

Template.propTypes = propTypes;
Template.defaultProps = defaultProps;

export default Template;
