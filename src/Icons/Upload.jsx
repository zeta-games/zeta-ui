import React from "react";
import PropTypes from "prop-types";

const propTypes = {
  /**
   * @default '24'
   */
  size: PropTypes.number,

  /**
   * @default '#000'
   */
  color: PropTypes.string
};

const defaultProps = {
  size: 24,
  color: "var(--bs-blue-10)"
};

const Upload = ({ size, color, ...props }) => (
  <svg
    fill="none"
    width={size}
    height={size}
    viewBox="0 0 24 24"
    xmlns="http://www.w3.org/2000/svg"
    {...props}
  >
    <path
      stroke={color}
      strokeWidth="2"
      d="M17 11L12 6L7 11"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
    <path
      stroke={color}
      strokeWidth="2"
      d="M17 18L12 13L7 18"
      strokeLinecap="round"
      strokeLinejoin="round"
    />
  </svg>
);

Upload.propTypes = propTypes;
Upload.defaultProps = defaultProps;

export default Upload;
