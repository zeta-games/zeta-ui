import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

/**
 * prop definition
 */
const propTypes = {
  /**
   * Background color of badge. If set, will override variant of badge selected
   * @default null
   */
  bg: PropTypes.string,

  /**
   * Color of text in badge, color will only apply when isStatus is false
   * @default "#fff"
   */
  color: PropTypes.string,

  /**
   * Toggle between whether badge is a status or not. If false, color is required
   * @default false
   */
  isStatus: PropTypes.bool,

  /**
   * Background opacity of badge
   */
  opacity: PropTypes.number,

  /**
   * Variant of badge. Will be overridden by bg props if set
   */
  variant: PropTypes.oneOf([
    "stale",
    "failed",
    "success",
    "warning",
    "pending",
    "processing"
  ]),

  /**
   * Border radius of badge
   */
  radius: PropTypes.number
};

const defaultProps = {
  color: "#fff",
  isStatus: true,
  opacity: 0.1,
  radius: 200
};

const Badge = ({
  bg,
  color,
  radius,
  variant,
  opacity,
  isStatus,
  children,
  ...props
}) => {
  /**
   * variables
   */
  bg = ((bg, variant) => {
    if (bg) {
      return bg;
    }

    switch (variant) {
      case "failed":
        return "var(--bs-red)";
      case "success":
        return "var(--bs-green-30)";
      case "processing":
        return "var(--bs-blue-40)";
      case "warning":
      case "pending":
        return "var(--bs-orange)";
      case "stale":
        return "var(--bs-grey-40)";
      default:
        break;
    }
  })(bg, variant);

  return (
    <Wrapper {...{ ...props, bg, color, isStatus, opacity, radius }}>
      {isStatus && <span className="dot" />}
      <span className="text-sub -medium">{children}</span>
    </Wrapper>
  );
};

/**
 * styles
 */
const Wrapper = styled.div`
  z-index: 1;
  gap: 0.25rem;
  line-height: 1;
  overflow: hidden;
  font-weight: 500;
  position: relative;
  align-items: center;
  white-space: nowrap;
  font-size: 0.6875rem;
  display: inline-flex;
  background-color: #fff;
  padding: 0.5rem 0.75rem;
  border-radius: ${({ radius }) => `${radius * 0.0625}rem`};

  span.dot {
    width: 0.5rem;
    height: 0.5rem;
    border-radius: 50%;
    background-color: ${({ bg }) => bg};
  }

  span.text-sub {
    display: block;
    margin-top: 1px;
    color: ${({ isStatus, color, bg }) => (isStatus ? bg : color)};
  }

  &::before {
    opacity: ${({ opacity }) => opacity};
    background-color: ${({ bg }) => bg};
    position: absolute;
    display: block;
    content: " ";
    height: 100%;
    width: 100%;
    z-index: -1;
    right: 0;
    top: 0;
  }
`;

Badge.propTypes = propTypes;
Badge.defaultProps = defaultProps;

export default Badge;
