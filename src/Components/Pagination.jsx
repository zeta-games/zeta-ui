import React from "react";
import PropTypes from "prop-types";
import Paginate from "react-paginate";
import styled from "styled-components";

import ChevronIcon from "../Icons/Chevron";

/**
 * props definition
 */
const propTypes = {
  totalPages: PropTypes.number.isRequired,
  setPage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired
};

const defaultProps = {
  page: 0,
  totalPages: 0,
  setPage: () => {}
};

const Template = ({ totalPages, page, setPage, ...props }) => (
  <Wrapper {...props}>
    <Paginate
      initialPage={page}
      pageCount={totalPages}
      pageRangeDisplayed={1}
      marginPagesDisplayed={2}
      onPageChange={({ selected }) => setPage(selected)}
      nextLabel={<ChevronIcon direction="right" color="var(--bs-grey-30)" />}
      previousLabel={<ChevronIcon direction="left" color="var(--bs-grey-30)" />}
    />
  </Wrapper>
);

/**
 * styles
 */
const Wrapper = styled.div`
  overflow-x: auto;

  ul {
    padding: 0rem;
    display: flex;
    list-style: none;
    margin-bottom: 0rem;

    & > *:not(:last-child) {
      margin-right: 0.25rem;
    }

    li {
      a {
        border: solid 1px var(--bs-grey-60);
        color: var(--bs-grey-30) !important;
        transition: ease-in-out all 0.15s;
        justify-content: center;
        border-radius: 0.125rem;
        align-items: center;
        font-size: 0.875rem;
        min-width: 2rem;
        padding: 0 0.5rem;
        height: 2rem;
        display: flex;
        outline: none;
      }

      &.next,
      &.previous {
        a {
          padding: 0;
        }

        &.disabled {
          a {
            svg {
              path {
                fill: var(--bs-grey-50);
              }
            }
          }
        }
      }

      &.selected {
        a {
          color: #fff !important;
          transition: ease all 0.25s;
          border-color: var(--bs-green-30);
          background-color: var(--bs-green-30);
        }
      }
    }
  }
`;

Template.propTypes = propTypes;
Template.defaultProps = defaultProps;

export default Template;
