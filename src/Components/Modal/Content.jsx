import React from "react";
import Modal from "react-bootstrap/Modal";

import LinkComponent from "../Link";
import CloseIcon from "../../Icons/Close";

const Content = ({ children, onHide }) => (
  <Modal.Body className="p-0">
    <div className="d-flex mb-1">
      <LinkComponent onClick={onHide} className="d-block ml-auto p-2">
        <CloseIcon color="var(--bs-blue-10)" />
      </LinkComponent>
    </div>
    {children}
  </Modal.Body>
);

export default Content;
