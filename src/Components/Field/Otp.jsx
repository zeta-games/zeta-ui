import React from "react";
import PropTypes from "prop-types";
import OtpInput from "react-otp-input";
import styled from "styled-components";

import ErrorMessage from "./Error";
import loopUtil from "../../Utils/loop";

const propTypes = {
  value: PropTypes.string,
  withFormik: PropTypes.bool,
  numInputs: PropTypes.number,
  name: PropTypes.string.isRequired,
  setFieldValue: PropTypes.func.isRequired,
  setFieldTouched: PropTypes.func.isRequired
};

const defaultProps = {
  value: "",
  numInputs: 6,
  withFormik: false,
  setFieldValue: () => {},
  setFieldTouched: () => {}
};

/**
 * otp
 *
 * @param {*} value value for otp input
 * @param {*} numInputs number of OTP inputs to be rendered
 * @param {*} withFormik use with formik
 */

const Otp = ({
  name,
  value,
  numInputs,
  withFormik,
  setFieldValue,
  setFieldTouched,
  wrapperClassName,
  ...props
}) => {
  return (
    <Wrapper className={wrapperClassName} {...{ numInputs }}>
      <OtpInput
        isInputNum
        shouldAutoFocus
        {...{
          value,
          numInputs,
          inputStyle: "otp-input",
          containerStyle: "otp-container",
          placeholder: loopUtil(numInputs, "*"),
          onChange: (input) => {
            setFieldValue(name, input);

            if (input?.length === numInputs) {
              setTimeout(() => setFieldTouched(name, true));
            }
          },
          ...props
        }}
      />
      {withFormik && <ErrorMessage name={name} className="text-red mt-2" />}
    </Wrapper>
  );
};

/**
 * styles
 */
const Wrapper = styled.div`
  .otp-container {
    width: 100%;
    gap: 0.625rem;
    margin: 0 auto;
    justify-content: center;
    display: grid !important;
    grid-template-columns: repeat(${({ numInputs }) => numInputs}, 1fr);
    max-width: ${({ numInputs }) => `${(numInputs * 48 + 10 * 5) * 0.0625}rem`};

    .otp-input {
      padding: 0;
      outline: none;
      line-height: 1;
      height: 2.75rem;
      font-size: 1rem;
      font-weight: 500;
      border-radius: 0.75rem;
      width: 100% !important;
      color: var(--bs-blue-10);
      border: solid 1px var(--bs-grey-60);

      &:focus {
        border-color: var(--bs-blue-10);
      }

      &:not(:placeholder-shown) {
        border-color: var(--bs-blue-10);
      }

      &:disabled {
        opacity: 0.65;
        background-color: transparent;
        border-color: var(--bs-grey-60);
      }

      &::placeholder {
        color: var(--bs-grey-50);
      }
    }
  }
`;

Otp.propTypes = propTypes;
Otp.defaultProps = defaultProps;

export default Otp;
