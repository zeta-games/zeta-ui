import React, { useState } from "react";
import { getCountries, formatPhoneNumberIntl, getCountryCallingCode } from "react-phone-number-input"; // prettier-ignore
import { Field } from "formik";
import countryList from "react-select-country-list";
import PropTypes from "prop-types";
import Dropdown from "react-bootstrap/Dropdown";
import styled from "styled-components";
import Phone from "react-phone-number-input/input";
import Flag from "react-country-flag";
import Form from "react-bootstrap/Form";

import { DefaultWrapper, FloatWrapper } from "./Wrapper";
import ChevronIcon from "../../Icons/Chevron";

/**
 * props definition
 */
const propTypes = {
  float: PropTypes.bool,
  withFormik: PropTypes.bool,
  placeholder: PropTypes.string,
  onlyCountries: PropTypes.array,
  defaultCountry: PropTypes.string,
  name: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  setFieldValue: PropTypes.func.isRequired,
  setFieldTouched: PropTypes.func.isRequired
};

const defaultProps = {
  float: false,
  placeholder: "",
  withFormik: true,
  setFieldValue: () => {},
  setFieldTouched: () => {}
};

const PhoneInput = ({
  name,
  float,
  label,
  value,
  disabled,
  withFormik,
  placeholder,
  onlyCountries,
  setFieldValue,
  defaultCountry,
  setFieldTouched,
  wrapperClassName,
  containerClassName,
  ...props
}) => {
  /**
   * states
   */
  const [country, setCountry] = useState(defaultCountry?.toUpperCase());

  /**
   * functions
   */
  const handleCountries = () => {
    const selectedCountries = onlyCountries || getCountries();

    return selectedCountries
      .map((country) => {
        if (!country) return false;

        country = country.toUpperCase();

        const countryName = countryList().getLabel(country);

        if (countryName) {
          return {
            country,
            value: countryName,
            label: getCountryCallingCode(country)
          };
        }

        return {};
      })
      .filter((e) => e)
      .sort((a, b) => {
        const nameA = a?.value?.toUpperCase();
        const nameB = b?.value?.toUpperCase();

        if (nameA < nameB) {
          return -1;
        }
        if (nameA > nameB) {
          return 1;
        }

        // names must be equal
        return 0;
      });
  };

  /**
   * variables
   */
  const Parent = float ? FloatWrapper : DefaultWrapper;

  return (
    <div>
      <Form.Label>{label}</Form.Label>
      <div className="d-flex gap-2">
        <Dropdown>
          <Toggle
            className={`rounded-0 justify-content-center px-0 gap-2 ${
              disabled && "bg-grey-70"
            }`}
          >
            {country && (
              <div className="d-flex gap-2 align-items-center">
                <Flag
                  svg
                  countryCode={country}
                  style={{ width: "1.25rem", height: "1rem" }}
                />
                <span>+{country && getCountryCallingCode(country)}</span>
              </div>
            )}
            <ChevronIcon
              size={20}
              direction="down"
              color="var(--bs-blue-10)"
              style={{ flex: "0 0 1.25rem" }}
            />
          </Toggle>
          <Menu className="text-truncate py-4 px-0">
            {handleCountries().map(({ label, value, ...others }, key) => {
              if (!label || !value) return false;

              return (
                <Item
                  key={key}
                  className={`text-truncate ${
                    country?.toLowerCase() === others?.country?.toLowerCase()
                      ? "active"
                      : ""
                  }`}
                  onClick={() =>
                    setCountry(others.country) |
                    setFieldValue("country", others.country) |
                    setFieldValue(name, "")
                  }
                >
                  <Flag countryCode={others.country} svg />
                  <span className="ml-1">{value}</span>
                  <span aria-label={others.country}>(+{label})</span>
                </Item>
              );
            })}
          </Menu>
        </Dropdown>
        <Parent
          {...{
            name,
            withFormik,
            wrapperClassName,
            containerClassName
          }}
        >
          <Field
            component={Phone}
            className="form-control"
            onBlur={() => setFieldTouched(name, true)}
            onChange={(value) =>
              setFieldValue(
                name,
                formatPhoneNumberIntl(value)?.replace(/\s/g, "")
              )
            }
            {...(country && { country })}
            {...{ name, value, placeholder, disabled }}
          />
        </Parent>
      </div>
    </div>
  );
};

/**
 * styles
 */

const Item = styled(Dropdown.Item)``;

const Menu = styled(Dropdown.Menu)`
  max-height: 25rem;
  overflow-y: auto;
  max-width: 20rem;

  .dropdown-item {
    gap: 0.5rem;
    display: flex;
    align-items: center;
    padding: 0.5rem 1rem;
  }
`;

const Toggle = styled(Dropdown.Toggle)`
  width: 7rem;
  height: 3rem;
  font-weight: 400;
  background-color: #fff;
  border-radius: 0.75rem;
  color: var(--bs-blue-10);
  border: solid 1px var(--bs-grey-60);
`;

PhoneInput.propTypes = propTypes;
PhoneInput.defaultProps = defaultProps;

export default React.memo(PhoneInput);
